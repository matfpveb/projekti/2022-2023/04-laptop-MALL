import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductInfoComponent } from './products/product-info/product-info.component';
import { FilterComponent } from './filter/filter.component'
import { ContactComponent } from './contact/contact.component';
import { StoresComponent } from './stores/stores.component';
import { LoginComponent } from './login/login.component';
import { DetailsComponent } from './details/details.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { UserInfoComponent } from './user-info/user-info.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ProductSumPipe } from './pipes/product-sum.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductInfoComponent,
    FilterComponent,
    ContactComponent,
    StoresComponent,
    LoginComponent,
    DetailsComponent,
    SearchBarComponent,
    SignupComponent,
    UserInfoComponent,
    NavMenuComponent,
    FavouritesComponent,
    ShoppingCartComponent,
    ProductSumPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
