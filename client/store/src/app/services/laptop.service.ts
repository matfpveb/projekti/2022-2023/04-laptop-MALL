import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filter, map, Observable, Subscription } from 'rxjs';
import { ProductPagination } from '../models/product-pagination';
import { Product } from '../models/product.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LaptopService {

  result : Observable<Product[]>;
  favouriteLaptops: Product[] = [];
  orderLaptops: Product[] = [];
  username: string = '';
  public user : User | null = null;
  private userSub : Subscription;
  hasNextPage : boolean = true;
  hasPreviousPage : boolean = true;
  subs: Subscription[] = [];



  constructor(private http : HttpClient, private auth : AuthService) {
    this.result = new Observable<Product[]>();
    this.userSub = this.auth.user.subscribe((user: User | null) => {this.user = user});
  }

  public get isBasketEmpty(): boolean {
    return this.orderLaptops.length === 0;
  }

  refreshList(): void {
    let x = this.getOrderLaptops().subscribe(data => {
      this.orderLaptops = data;
    });

    this.subs.push(x);
  }

  public getAllLaptops(page: number = 1, limit: number = 9) : Observable< Product[] > {
    const params: HttpParams = new HttpParams().append('page', page).append('limit', limit);
    const obs: Observable<ProductPagination> = this.http.get<ProductPagination>("http://localhost:9800/api/laptops", { params });
    return obs.pipe(
      map((pagination : ProductPagination) => {
        return pagination.docs;
      })
    );
  }


  public filterLaptops(page: number = 1, limit: number = 9, brends: String[], rams: String[], processors : String[], price: number, ssds: String[]): Observable<Product[]> {
    const params: HttpParams = new HttpParams()
      .append('page', page)
      .append('limit', limit)
    const obs: Observable<ProductPagination> = this.http.post<ProductPagination>("http://localhost:9800/api/laptops/filter", {brends, rams, processors, price, ssds} , { params });
    return obs.pipe(
      map((pagination: ProductPagination) => {
        this.hasNextPage = pagination.hasNextPage;
        this.hasPreviousPage = pagination.hasPrevPage;

        return pagination.docs;
    })
  );
}

  public findLaptopByName(page: number = 1, limit: number = 9, name: String): Observable<Product[]> {
    const params: HttpParams = new HttpParams()
      .append('page', page)
      .append('limit', limit)
    const obs: Observable<ProductPagination> = this.http.post<ProductPagination>("http://localhost:9800/api/laptops/search", {name}, { params });
    return obs.pipe(
      map((pagination: ProductPagination) => {
        return pagination.docs;
      })
    );
 }


  public getDetails(laptopId: number) : Observable<Product> {
    return this.http.get<Product>("http://localhost:9800/api/laptops/laptopId/" + String(laptopId));
  }

  getFavouriteLaptops() : Observable<Product[]> {
    return this.http.get<Product[]>("http://localhost:9800/api/favourites/" + this.user?.username);
  }


  addToFavourites(laptopName: string,id:number) : Observable<Product[]> {
    return this.http.put<Product[]>("http://localhost:9800/api/favourites/", { "username": this.user?.username, "laptopName" : laptopName, "id" : id}, {'headers': { 'content-type': 'application/json'}});
  }

  removeFromFavourites(laptopName: string, id:number) : Observable<Product[]> {
    return this.http.put<Product[]>("http://localhost:9800/api/favourites/" + this.user?.username, { "laptopName" : laptopName}, {'headers': { 'content-type': 'application/json'}});
  }

  getOrderLaptops() : Observable<Product[]> {
    
    return this.http.get<Product[]>("http://localhost:9800/api/orders/" + this.user?.username);
  }


  addToOrderLaptops(laptopName: string , id:number, price: number) : Observable<Product[]> {
    return this.http.put<Product[]>("http://localhost:9800/api/orders/", { "username": this.user?.username, "laptopName" : laptopName, "id" : id, "price" : price}, {'headers': { 'content-type': 'application/json'}});
  }

  removeFromOrderLaptops(laptopName: string, id:number, price: number) : Observable<Product[]> {
    return this.http.put<Product[]>("http://localhost:9800/api/orders/" + this.user?.username, { "laptopName" : laptopName, "price" : price}, {'headers': { 'content-type': 'application/json'}});
  }

  

}


