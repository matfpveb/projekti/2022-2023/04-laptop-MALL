import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Product } from '../models/product.model';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { LaptopService } from '../services/laptop.service';

declare const $: any;

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy{

  subs: Subscription[] = [];

  products : Observable<Product[]> = new Observable<Product[]>();
  public user : User | null = null;
  private userSub : Subscription;
  orderLaptops: Product[];
  public isLogin : boolean = false;



  constructor(private productService : LaptopService, public auth : AuthService, private router : Router){
    this.userSub = this.auth.user.subscribe((user : User | null) => {this.user = user});
    this.auth.sendUserDataIfExists();
    this.products = this.productService.getAllLaptops();
  }



  showFilteredLaptops(data : Observable<Product[]>){
    this.products = data;
  }

  showSearchedLaptops(data : Observable<Product[]>){
    this.products = data;
  }

  showFavouriteLaptop(data : Observable<Product[]>){
    this.products = data;
  }

  onClickCart(): void{
    this.productService.refreshList();
    let x = this.productService.getOrderLaptops().subscribe(data => {
      this.orderLaptops = data;
    });

    this.subs.push(x);
  } 

  ngOnInit(): void {
    $('.menu .item').tab()
    $('#dropdown2').dropdown();
  }

  logOut() : void {
    this.auth.logout();
    this.router.navigateByUrl('/');
  }

  ngOnDestroy(): void {
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }

  ngAfterViewChecked(): void {
    $('.menu .item').tab();
    $('#dropdown1').dropdown();
  }

}
