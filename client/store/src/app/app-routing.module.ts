import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent} from './signup/signup.component'
import { LoginComponent } from './login/login.component';
import { DetailsComponent } from './details/details.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

const routes: Routes = [
  {path: '', component: NavMenuComponent},
  {path: 'register', component : SignupComponent},
  {path: 'login', component : LoginComponent},
  {path: 'details', component: DetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
