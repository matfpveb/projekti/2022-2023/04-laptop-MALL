import { Router } from '@angular/router';
import { LaptopService } from 'src/app/services/laptop.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model'
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { UsernameValidator } from '../validators/username.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  sub: Subscription = new Subscription();
  loginForm: FormGroup;
  user: User;


  constructor(private formBuilder: FormBuilder, private auth:AuthService,
    private laptopService: LaptopService,
    private router:Router) {

    this.user = new User('peraperic', 'pera123');

    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(2), UsernameValidator]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)])
    })

  }
   
  ngOnDestroy(): void {
      this.sub ? this.sub.unsubscribe() : null;
  }

  ngOnInit(): void {
  }

  onLogin(): void {

    const data = this.loginForm.value;

    if(!this.formValidation()){
      this.loginForm.reset();
      return;
    }

    this.sub = this.auth.login(data.username, data.password).subscribe((user: User | null) => {
      if(user !== null){
        this.auth.username = user.username;
        this.router.navigateByUrl('/');
      }
      else{
        this.loginForm.reset();
      }
    });

  }

  private formValidation() : boolean {

    const usernameErrors: ValidationErrors | null | undefined = this.loginForm.get('username')?.errors;
    if(usernameErrors !== null ){
      window.alert('Invalid username');
      return false;
    }

    const passwordErrors: ValidationErrors | null | undefined = this.loginForm.get('password')?.errors;
    if( passwordErrors !== null ){
      window.alert('Invalid password');
      return false;
    }

    if(this.loginForm.invalid){
      window.alert('Invalid input');
      return false;
    }

    return true;
  }


  public nameHasErrors() : boolean {
    const errors : ValidationErrors | null | undefined = this.loginForm.get("username")?.errors;
    return errors !== null;
  }

  public passwordHasErrors() : boolean {
    const errors : ValidationErrors | null | undefined = this.loginForm.get("password")?.errors;
    return errors !== null;
  }

  public nameErrors() : string[] {
    const errors : ValidationErrors | null | undefined = this.loginForm.get("username")?.errors;
    if(errors == null){
      return [];
    }
    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Username is required!');
    }

    if(errors['minlength']) {
      errorMessages.push(`Username is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`);
    }

    if(errors['UsernameValidator']){
      errorMessages.push(errors['UsernameValidator'].message);
    }
  
     return errorMessages;
  }


  public passwordErrors(): string[] {
    const errors: ValidationErrors | null | undefined = this.loginForm.get('password')?.errors;

    if (errors == null) {
      return [];
    }
    const errorMessages: string[] = [];

    if(errors['required']){
      errorMessages.push('Password is required!');
    }

    if(errors['minlength']) {
      errorMessages.push(`Password should be at least ${errors['minlength'].requiredLength} characters!`);
    }

    return errorMessages;
  }


}
