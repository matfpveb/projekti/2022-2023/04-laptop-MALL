import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

declare const $: any;

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})


export class UserInfoComponent implements OnInit, OnDestroy{

  private sub : Subscription = new Subscription();
  public user : User | null = null;
  public isLogin : boolean = false;

  constructor(public auth: AuthService, private router : Router){
    this.sub = this.auth.user.subscribe((user : User | null) => {this.user = user});
    this.auth.sendUserDataIfExists();
    
  }

  ngOnInit(): void {
    $('#dropdown1').dropdown();
  }

  logOut() : void {
    this.auth.logout();
    window.location.reload();
  }

  ngOnDestroy(): void {
      if(this.sub){
        this.sub.unsubscribe();
      }
  }
}
