import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { filter, Observable } from 'rxjs';
import { Product } from '../models/product.model';
import { LaptopService } from '../services/laptop.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit{

  @Output() productsForSend : EventEmitter<Observable<Product[]>> = new EventEmitter<Observable<Product[]>>();
  searchForm : FormGroup;
  @Input() products: Observable<Product[]> | null;

  ngOnInit(): void {
  }
  
  constructor(private laptopService : LaptopService){
    this.searchForm = new FormGroup({
      searchField: new FormControl('', [])
    });
  }

  onSubmit(event: any){
    const data = this.searchForm.value;
    const name = data.searchField;
    if(event.keyCode == 13) {
      let x = this.laptopService.findLaptopByName(1, 9, name);
      this.productsForSend.emit(x);
      this.searchForm.reset();
    }
  }
}
