import { Component, OnInit, Input} from '@angular/core';
import { Observable } from 'rxjs';
import { LaptopService } from 'src/app/services/laptop.service';
import { Product } from '../../models/product.model';


declare const $ : any;

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit{

  @Input() products: Observable<Product[]>;
  currentPage : number = 1;

  ngOnInit(): void {
  }

  constructor(private laptopService : LaptopService){
    this.products = this.laptopService.getAllLaptops(this.currentPage);
  }
}
