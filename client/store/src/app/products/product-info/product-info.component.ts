import { LaptopService } from 'src/app/services/laptop.service';
import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../models/product.model';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit{

  @Input() product: Product;

  showChangeFields: boolean;
  inFavourites: boolean;


  constructor(private laptopService: LaptopService,
              private auth : AuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router
  ){
    this.product = new Product(16, "ASUS", "ASUS", "Intel", "16", "512", 1500, '..');
    this.showChangeFields = false;
  }


  enableChangeFields() {

    this.showChangeFields = true;
  }

  disableChangeFields() {

    this.showChangeFields = false;
  }

  onHeartClicked(): void{
    if(this.laptopService.user == null){
      window.alert("You must be a logged in user!");
      return;
    }

    if(this.inFavourites){
      this.laptopService.removeFromFavourites(this.product.name,this.product.id).subscribe();
    }
    else{
      this.laptopService.addToFavourites( this.product.name,this.product.id).subscribe();
    }

    this.inFavourites = !this.inFavourites;

  }


  ngOnInit(): void {
    this.laptopService.getFavouriteLaptops().subscribe(data=>{
      this.inFavourites = data.findIndex(x=> x.name == this.product.name) !== -1;
    })

  }

  public addToCart(): void{
    if(this.laptopService.user == null){
      this.router.navigateByUrl('/login');
    }else{
      this.laptopService.addToOrderLaptops(this.product.name, this.product.id, this.product.price).subscribe();
    }
  } 
}
