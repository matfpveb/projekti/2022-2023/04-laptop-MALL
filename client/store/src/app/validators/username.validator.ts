import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const UsernameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    if(control.value !== null){
    const nameParts: string[] = control.value.trim().split(' ')
        .filter((namePart: string) => namePart.trim().length > 0);

    if (nameParts.length === 0) {
        return {
            UsernameValidator: {
                message: "Username cannot consist of whitespaces only!"
            }
        }
    }

    if (nameParts.every((namePart: string) => namePart.match(new RegExp("^[0-9\s]+$")))) {
        return {
            UsernameValidator: {
                message: "Username cannot be only digits!"
            }
        }
    }
    }

    return null;
}