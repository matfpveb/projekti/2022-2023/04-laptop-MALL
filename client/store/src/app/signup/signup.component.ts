import { LaptopService } from '../services/laptop.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { UsernameValidator } from '../validators/username.validator';
;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit,OnDestroy {
  sub: Subscription = new Subscription();
  user: User | undefined;
  signUpForm: FormGroup;


  constructor(private formBuilder: FormBuilder,
            private laptopService: LaptopService,
            private auth:AuthService,
            private router: Router) {

    this.signUpForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(3), UsernameValidator]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)]),
      confirmPassword: new FormControl('', [Validators.required])
    })

   }
  ngOnInit(): void {
  }

  ngOnDestroy() : void {
    if(this.sub)
      this.sub.unsubscribe();
  }

  onSignUp() {

    const data = this.signUpForm.value;

    if(!this.formValidation()){
      this.signUpForm.reset();
      return;
    }

    const obs:Observable<User|null> = this.auth.registerUser(data.username, data.password);

    this.sub = obs.subscribe((user:User|null)=>{
      if(user !== null){
        
      }

      this.router.navigateByUrl('/login');

    });
  }

  private formValidation() : boolean {

    const data = this.signUpForm.value;
    const usernameErrors: ValidationErrors | null | undefined = this.signUpForm.get('username')?.errors;
    if(usernameErrors !== null ){
      window.alert('Invalid username');
      return false;
    }

    const passwordErrors: ValidationErrors | null | undefined = this.signUpForm.get('password')?.errors;
    if( passwordErrors !== null ){
      window.alert('Invalid password');
      return false;
    }

    if(data.password !== data.confirmPassword){
      window.alert('Password and confirmed password do not match');
      return false;
    }


    if(this.signUpForm.invalid){
      window.alert('Invalid input');
      return false;
    }

    return true;
  }
  

  public nameHasErrors() : boolean {
    const errors : ValidationErrors | null | undefined = this.signUpForm.get("username")?.errors;
    return errors !== null;
  }

  public passwordHasErrors() : boolean {
    const errors : ValidationErrors | null | undefined = this.signUpForm.get("password")?.errors;
    return errors !== null;
  }

  public nameErrors() : string[] {
    const errors : ValidationErrors | null | undefined = this.signUpForm.get("username")?.errors;
    if(errors == null){
      return [];
    }
    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Username is required!');
    }

    if(errors['minlength']) {
      errorMessages.push(`Username is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`);
    }

    if(errors['UsernameValidator']){
      errorMessages.push(errors['UsernameValidator'].message);
    }
  
     return errorMessages;
  }


  public passwordErrors(): string[] {
    const errors: ValidationErrors | null | undefined = this.signUpForm.get('password')?.errors;

    if (errors == null) {
      return [];
    }
    const errorMessages: string[] = [];

    if(errors['required']){
      errorMessages.push('Password is required!');
    }

    if(errors['minlength']) {
      errorMessages.push(`Password should be at least ${errors['minlength'].requiredLength} characters!`);
    }

    return errorMessages;
  }
}