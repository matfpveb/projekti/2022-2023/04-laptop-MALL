import { LaptopService } from 'src/app/services/laptop.service';
import { Product } from 'src/app/models/product.model';
import { Component,EventEmitter,Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';


declare const $: any;

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent {


  @Output() laptopsEvent: EventEmitter<Observable<Product[]>> = new EventEmitter<Observable<Product[]>>();
  products: Product[];
  favouriteLaptops: Product[] = [];

  subs: Subscription[] = [];

  constructor(private laptopService: LaptopService) {

    this.products = [];
    let x = laptopService.getFavouriteLaptops().subscribe(data => {
      this.favouriteLaptops = data;
      this.laptopService.favouriteLaptops = data;
    });
    this.subs.push(x);
  }

  onClick(name: string): void {
    let x = this.laptopService.findLaptopByName(1,9,name);
    this.laptopsEvent.emit(x);
   }

   refreshList() : void {
    let x = this.laptopService.getFavouriteLaptops().subscribe(data => {
      this.favouriteLaptops= data;
    });
    this.subs.push(x);
   }

  ngOnInit(): void {
    $('#dropdown2').dropdown();
  }

  ngOnDestroy(): void {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }




}
