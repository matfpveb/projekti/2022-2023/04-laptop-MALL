import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormArray, FormControl, FormGroup} from '@angular/forms';
import { Observable, Subscription} from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { LaptopService } from 'src/app/services/laptop.service';

declare const $: any;

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy{

  @Output() productsForSend : EventEmitter<Observable<Product[]>> = new EventEmitter<Observable<Product[]>>();
  products : Observable<Product[]>;
  filterForm: FormGroup;
  brendLaptops : String[] = [];
  ramLaptops : String[] = [];
  processorLaptops : String[] = [];
  subs: Subscription[] = [];
  price: string = "";
  priceNumber : number = 0;
  ssdLaptops : String[] = [];
  page : number = 1;
  hasNextPage : boolean = true;
  hasPreviousPage : boolean = true;

  brends: String[] = [
    "Lenovo", "Hp", "Dell", "Samsung"
  ];

  rams: String[] = [
    "4GB", "8GB", "12GB", "16GB"
  ];

  procesors: String[] = [
    "Intel", "Ryzen"
  ];

  ssds: String[] = [
    "256GB", "512GB"
  ];

  constructor(private laptopService : LaptopService){
    this.products = new Observable<Product[]>();
    this.filterForm = new FormGroup({
      brendForm: new FormArray([]),
      ramForm: new FormArray([]),
      procesorForm: new FormArray([]),
      price: new FormControl(),
      ssdForm: new FormArray([])
    });
    this.addCheckboxes();
  }
  ngOnInit(): void {
    $('.ui.radio.checkbox').checkbox();
  }

  get brendsFormArray(): FormArray {
    return this.filterForm.get("brendForm") as FormArray;
  }

  get ramsFormArray() : FormArray {
    return this.filterForm.get("ramForm") as FormArray;
  }

  get procesorsFormArray() : FormArray {
    return this.filterForm.get("procesorForm") as FormArray;
  }

  get ssdsFormArray() : FormArray {
    return this.filterForm.get("ssdForm") as FormArray;
  }

  private addCheckboxes() {
    this.brends.forEach(() => this.brendsFormArray.push(new FormControl(false)));
    this.rams.forEach(() => this.ramsFormArray.push(new FormControl(false)));
    this.procesors.forEach(() => this.procesorsFormArray.push(new FormControl(false)));
    this.ssds.forEach(() => this.ssdsFormArray.push(new FormControl(false)));
  }


  public onFilter() : void {
    const data = this.filterForm.value;
    const selectedBrands = data.brendForm;
    const selectedRams = data.ramForm;
    const selectedProcessors = data.procesorForm;
    const selectedSsds = data.ssdForm;

    this.brendLaptops = [];
    this.ramLaptops = [];
    this.processorLaptops = [];
    this.ssdLaptops = [];

    for(let i = 0; i < this.brends.length; i++){
      if(selectedBrands[i]){
        this.brendLaptops.push(this.brends[i]);
      }
    }

    for(let i = 0; i < this.rams.length; i++){
      if(selectedRams[i]){
        this.ramLaptops.push(this.rams[i]);
      }
    }

    for(let i = 0; i < this.procesors.length; i++){
      if(selectedProcessors[i]){
        this.processorLaptops.push(this.procesors[i]);
      }
    }

    for(let i = 0; i < this.ssds.length; i++){
      if(selectedSsds[i]){
        this.ssdLaptops.push(this.ssds[i]);
      }
    }

    if(this.brendLaptops.length === 0){
      this.brendLaptops = this.brends;
    }

    if(this.ramLaptops.length === 0){
      this.ramLaptops = this.rams;
    }

    if(this.processorLaptops.length === 0){
      this.processorLaptops = this.procesors;
    }

    if (data.price === null){
      this.priceNumber = 2499;
    }
    else{
      this.priceNumber = parseInt(data.price);
    }

    if(this.ssdLaptops.length === 0){
      this.ssdLaptops = this.ssds;
    }

    let x = this.laptopService.filterLaptops(this.page,9,this.brendLaptops, this.ramLaptops, this.processorLaptops, this.priceNumber, this.ssdLaptops);
    this.hasNextPage = this.laptopService.hasNextPage;
    this.hasPreviousPage = this.laptopService.hasPreviousPage;

    this.productsForSend.emit(x);
  }

  onNextClicked() {
    if(this.hasNextPage){
      this.page++;
      let x = this.laptopService.filterLaptops(this.page,9,this.brendLaptops, this.ramLaptops, this.processorLaptops, this.priceNumber, this.ssdLaptops);
      this.productsForSend.emit(x);
    }
  }

  onPreviousClicked() {
    if(this.hasPreviousPage){
      this.page--;
      let x = this.laptopService.filterLaptops(this.page,9,this.brendLaptops, this.ramLaptops, this.processorLaptops, this.priceNumber, this.ssdLaptops);
      this.productsForSend.emit(x);
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach(subscription => subscription.unsubscribe());
  }


}
