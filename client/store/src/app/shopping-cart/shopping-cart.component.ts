import { Component, Input} from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from '../models/product.model';
import { LaptopService } from '../services/laptop.service';
import { RouterLink } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent {

  products: Product[];
  @Input() orderLaptops: Product[] = [];
  bought : boolean = false;

  subs: Subscription[] = [];

  constructor(private laptopService: LaptopService, private router : RouterLink){
    this.laptopService.refreshList();
    this.products = [];
    let x = laptopService.getOrderLaptops().subscribe(data =>{
      this.orderLaptops = data;
      this.laptopService.orderLaptops = data;
    });
    
    this.subs.push(x);
  }

  public get isBasketEmpty(): boolean {
    if(this.laptopService.isBasketEmpty){
      this.bought = false;
    }
    return this.laptopService.isBasketEmpty;
  }


  removeOrderFromBasket(order: Product): void {
    this.laptopService.removeFromOrderLaptops(order.name, order.id, order.price).subscribe();
    this.laptopService.refreshList();
  }
  

  onBuyClick() : void{
    this.bought = true;
    this.orderLaptops.forEach((p : Product ) => { this.removeOrderFromBasket(p);});
    this.laptopService.refreshList();
  }

  ngOnInit(): void{
    this.laptopService.refreshList();
  }

  ngOnDestroy(): void{
    this.subs.forEach(subscription => subscription.unsubscribe());
  }

}
