export class Product {
    constructor(
        public id :  number,
        public name: string,
        public brand: string,
        public processor: string,
        public ram: string,
        public ssd :string,
        public price: number,
        public imgUrl : string,

    ){}
}
