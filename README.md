# <img src="assets/laptop.png" width="130"/>           Project laptop MALL 



# Opis :memo: :

Web aplikacija koja nudi veliki broj laptopova razlicitih brendova. Moguce je pretrazivati laptopove po nazivu ili filtrirati rezultate prema odredjenim karakteristikama. Korisnik moze da se registruje, pretrazuje i pravi svoju listu omiljenih laptopova.


# Koriscene tehnologije :

![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)
![](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![](https://img.shields.io/badge/Semantic_UI-563D7C?style=for-the-badge&logo=semanticui&logoColor=white)



# Instalacija :hammer: :

Node se moze preuzeti na [ovom](https://nodejs.org/en/download/) linku.
MongoDB se moze preuzeti na [ovom](https://www.mongodb.com/try/download/community) linku.


# Preuzimanje i pokretanje :wrench: :


Aplikacija se moze pokrenuti koriscenjem skripta:

1. U terminalu se pozicionirati u zeljeni direktorijum
2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matfpveb/projekti/2022-2023/04-laptop-MALL.git
3. Potrebno je pozicionirati se u `data` folder i pokrenuti sledece komande:
```
chmod +x importData.sh
./importData.sh
```
4. Pozicionirati se u `server` folder i pokrenuti sledece komande:
``` 
chmod +x startServer.sh
./startServer.sh
```
5. Pozicionirati se u `client/store` folder i pokrenuti sledece komande:
```
chmod +x startClient.sh
./startClient.sh
```

Aplikacija je pokrenuta na adresi http://localhost:4200/ 



___



Pokretanje aplikacije bez koriscenja skripta:

1. Pozicionirati se u  `server` folder i pokrenuti sledece komande:

``` 
npm install
node server.js
```

2. Pozicionirati se u `client/store` folder i pokrenuti sledece komande:
``` 
npm install
ng serve
```
3. U pretrazivacu otvoriti adresu http://localhost:4200/



# Schema baze podataka 
<table>
<tr>
<th>Users</th>
<th>Laptops</th>
<th>Favourites</th>
<th>Orders</th>
</tr>
<tr>
<td>

 Polje             | Tip       | Opis                               |
 ----------------- | ----------|------------------------------------|
 username          | String    |                                    |
 hash              | String    | podaci za cuvanje lozinke          |
 salt              | String    | podaci za cuvanje lozinke         
</td>
<td>

 Polje             | Tip      | Opis                                 |
 ------------------| ---------|--------------------------------------|
 _id               | ObjectId |                                      |
 name   | String  | naziv laptopa  |                                      |
 processor | String   |  tip procesora   |
 ram    | String   |  velicina RAM memorije |
 ssd    | String | velicina SSD-a |
 price  | Number | cena laptopa |
 imgUrl | String | slika laptopa |
</td>
<td>

 Polje      | Tip       | Opis                                                  |
 ---------  | ----------|-------------------------------------------------------|
 username   | String    |                                                       |
 laptops    | [laptop]| **id**  i  **naziv** svih laptopova koje je korisnik dodao u listu|
</td>
<td>

 Polje      | Tip       | Opis                                                         |
 ---------  | ----------|------------------------------------------------------------- |
 username   | String    |                                                              |
 laptops    | [laptop]|  **id** , **naziv** i **cena** svih laptopova koje je korisnik dodao u listu|
</td>
</tr>
</table>

___

# Demo snimak aplikacije :movie_camera: :

Demo snimak aplikacije mozete pogledati na [linku](https://www.youtube.com/watch?v=TzLrYM4aR0Y).




## Developers

- [Marija Čulić, 269/2018](https://gitlab.com/marijaculic)
- [Luka Radenković, 59/2018](https://gitlab.com/smajli99)
- [Anđela Stajić, 147/2018](https://gitlab.com/andjelastajic8)
- [Lidija Đalović, 107/2018](https://gitlab.com/lidijadjalovic)
