const express = require('express');
const router = express.Router();

const controller = require('../../controllers/laptops');


router.get('/', controller.getAllLaptops);

router.post('/search', controller.getLaptopByName);
 
router.post('/price', controller.getLaptopsByPrice);

router.post('/', controller.addNewLaptop);

router.post('/filter', controller.filterLaptops);


module.exports = router;