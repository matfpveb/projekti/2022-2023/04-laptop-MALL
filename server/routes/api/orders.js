const express = require('express');
const ordersController = require('../../controllers/orders');

const router = express.Router();

router.get('/:username', ordersController.getOrders);
router.put('/:username', ordersController.removeFromOrders);
router.put('/', ordersController.addToOrders);


module.exports = router;