const ordersService = require('../services/orders');


const getOrders = async(req, res, next) => {
    const username = req.params.username;
  
    try {
      if (username === undefined) {
        const error = new Error('Username missing');
        error.status = 400;
        throw error;
      }
  
      const data = await ordersService.getOrders(username);
      if (data === null) {
        res.status(404).json();
      } else {
        res.status(200).json(data);
      }
    } catch (error) {
      next(error);
    }
  };
  
  const addToOrders = async(req, res, next) => {
    const {username, laptopName, id, price } = req.body;
    
    try {
      if (!username  || !laptopName ) {
        const error = new Error('Invalid input');
        error.status = 400;
        throw error;
      }
  
      let newOrder = await ordersService.addToOrders(
        username,
        laptopName,
        id,
        price
      );

      if(!newOrder){
        newOrder = await ordersService.createNewOrders(
          username,
          laptopName,
          id,
          price
        )
      }
  
      if (newOrder) {
        res.status(200).json(newOrder);
      } else {
        const error = new Error('Invalid username or id');
        error.status = 403;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  };


  const removeFromOrders = async(req, res, next) => {
    const {laptopName, id, price} = req.body;
    const username = req.params.username;

    try {
      if (username === undefined ) {
        const error = new Error('Username od id missing');
        error.status = 400;
        throw error;
      }

      
  
      let data = await ordersService.removeFromOrders(username, laptopName, id, price);

     
      if (data === null) {
        res.status(404).json();
      } else {
        res.status(200).json(data);
      }
    } catch (error) {
      next(error);
    }
  };
  
  module.exports = {
      getOrders,
      addToOrders,
      removeFromOrders
  }
  
