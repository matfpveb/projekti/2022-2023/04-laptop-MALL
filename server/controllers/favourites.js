const favouritesService = require('../services/fvaourites');

const getFavourites = async(req, res, next) => {
    const username = req.params.username;
    try {
      if (username === undefined) {
        const error = new Error('Username missing');
        error.status = 400;
        throw error;
      }
  
      const data = await favouritesService.getFavourites(username);
      if (data === null) {
        res.status(404).json();
      } else {
        res.status(200).json(data);
      }
    } catch (error) {
      next(error);
    }
  };
  
  const addToFavourites = async(req, res, next) => {
    const {username, laptopName,id } = req.body;
    try {
      if (!username  || !laptopName ) {
        const error = new Error('Invalid input');
        error.status = 400;
        throw error;
      }
  
      let newFave = await favouritesService.addToFavourites(
        username,
        laptopName,
        id
      );


      if(!newFave){
        newFave = await favouritesService.createNewFavourites(
          username,
          laptopName,
          id
        )
      }
  
      if (newFave) {
        res.status(200).json(newFave);
      } else {
        const error = new Error('Invalid username or id');
        error.status = 403;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  };


  const removeFromFavourites = async(req, res, next) => {
    const {laptopName} = req.body;
    const username = req.params.username;

    try {
      if (username === undefined ) {
        const error = new Error('Username od id missing');
        error.status = 400;
        throw error;
      }     
  
      let data = await favouritesService.removeFromFavourites(username,laptopName,null);
           
      if (data === null) {
        res.status(404).json();
      } else {
        res.status(200).json(data);
      }
    } catch (error) {
      next(error);
    }
  };
  
  module.exports = {
      getFavourites,
      addToFavourites,
      removeFromFavourites
  }
  
