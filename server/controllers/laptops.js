const { default: mongoose} = require('mongoose');
const Laptop = require('../models/laptopsModel');

const getAllLaptops = async function (req, res, next) {
    const page = req.query.page;
    const limit = req.query.limit;

    try{
        const allLaptops = await Laptop.paginateThroughProducts(page, limit);
        res.status(200).json(allLaptops);
    } catch(err){
        next(err);
    }
};

const getLaptopByName = async  (req, res, next) => {
    let name = req.body.name;
    
    console.log(name);

    const laptop = await Laptop.paginateThroughProductsName(1, 9, name);
    res.status(200).json(laptop);
    console.log(laptop);
};


const filterLaptops = async (req,res,next) => {

    let brend = req.body.brends;
    let ram = req.body.rams;
    let processor = req.body.processors;
    let price = req.body.price;
    let ssd = req.body.ssds;
    const page = req.query.page;
    const limit = req.query.limit;

  
    try{
        if(brend == undefined){
            const err = new Error("Brend name is missing!");
            err.status = 400;
            throw err;
        }
    } catch(err){
        next(err);
    }

    
    const laptops = await Laptop.paginateThroughProductsFILTER(page, limit, brend, ram, processor, price, ssd);
    res.status(200).json(laptops);
    console.log(laptops);
};


const getLaptopsByPrice = async (req, res, next) => {
    try{
        const {lower, upper} = req.body;
        if(!lower || !upper){
            res.status(400).json();
        }
        else{
            const laptop = Laptop.find({price: {$gte: lower}, price: {$lte: upper}});
            if(laptop == null){
                res.status(404).json();
            }
            else{
                res.status(200).json(laptop);
            }
        } 
    } catch(err){
        next(err);
    }
};

const addNewLaptop = async (req, res, next) => {
    try{    
        const {name, brand, processor, ram, ssd,  price} = req.body;
        if(!name  || !brand || !processor || !ram || !ssd || !price){
            console.log(name, price);
            res.status(400).json();
        }
        else{
            const newLaptop = new Laptop({
                _id: new mongoose.Types.ObjectId(),
                name: name,
                brand: brand,
                processor: processor,
                ram: ram,
                ssd: ssd,
                price: price
            });
        
            await newLaptop.save();
            res.status(201).json(newLaptop);
        }
    } catch(err){
        next(err);
    }
};


module.exports = {
    getAllLaptops,
    getLaptopsByPrice,
    addNewLaptop,
    filterLaptops, 
    getLaptopByName
};
