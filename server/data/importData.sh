mongo projectBase --eval "db.dropDatabase()"
mongoimport --db projectBase --collection users --file users.json --jsonArray
mongoimport --db projectBase --collection laptops --file laptops.json --jsonArray
mongoimport --db projectBase --collection favourites --file favourites.json --jsonArray
mongoimport --db projectBase --collection orders --file orders.json --jsonArray
