const Favourites = require('../models/favouritesModels');

const getFavourites = async (username) => {
    const user = await Favourites.findOne({ username: username }).exec();
    if(user === null)
      return [];
    
    return user.laptops;
  }
  
const addToFavourites = async (username, name,id) => {
  await Favourites.findOneAndUpdate(
    {username : username},
    {$push: {laptops: {
      name : name,
      id: id
    }}},
    { useFindAndModify: false }
  );
  
  const newFave = await Favourites.findOne({username : username}).exec();
  if(newFave === null)
    return null;
  return newFave.laptops;
};

const createNewFavourites = async (username, name,id) => {
  
  const newFave = new Favourites({
    username : username,
    laptops : [{
      name : name,
      id : id,
    }]
});

await newFave.save();

return newFave.laptops;

}

//brisemo po imenu 
const removeFromFavourites = async (username,name,id) => {
 

await Favourites.findOneAndUpdate(
  { username : username }, 
  { $pull: { laptops: { name: name} } }
);

const data = await Favourites.findOne({username : username}).exec();

return data.laptops;
}


module.exports = {
    getFavourites,
    addToFavourites,
    createNewFavourites,
    removeFromFavourites
}
