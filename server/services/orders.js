const Orders = require('../models/ordersModel');

const getOrders = async (username) => {
    const user = await Orders.findOne({ username: username }).exec();
    if(user === null)
      return [];
    
    return user.laptops;
  }
  
const addToOrders = async (username, name, id, price) => {
  await Orders.findOneAndUpdate(
    {username : username},
    {$push: {laptops: {
      name : name,
      id: id,
      price: price
    }}},
    { useFindAndModify: false }
  );
  
  const newOrder = await Orders.findOne({username : username}).exec();
  if(newOrder === null)
    return null;
  return newOrder.laptops;5400
};

const createNewOrders = async (username, name, id, price) => {
  
  const newOrder = new Orders({
    username : username,
    laptops : [{
      name : name,
      id : id,
      price: price
    }]
});

await newOrder.save();

return newOrder.laptops;

}

const removeFromOrders = async (username,name,id, price) => {
 
  await Orders.findOneAndUpdate(
    { username : username }, 
    { $pull: { laptops: { name: name, price: price} } }
  );

const data = await Orders.findOne({username : username}).exec();

return data.laptops;
}

module.exports = {
    getOrders,
    addToOrders,
    createNewOrders,
    removeFromOrders
}
