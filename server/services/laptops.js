const mongoose = require('mongoose');
const Laptop = require('../models/laptopsModel');


const getAllUsers = async () => {
  const users = await User.find({}).exec();
  return users;
};

const getLaptopByBrend = async (brend) => {
  const laptops = await Laptop.find({brand : brend}).exec();
  return laptops;
};

const getLaptopByName = async(name) => {
  const laptops = await Laptop.paginateThroughProductsName(1, 9, name);
  return laptops;
};

module.exports = {
    getAllUsers,
    getLaptopByBrend,
    getLaptopByName
  };
  