const mongoose = require('mongoose');

const ordersSchema = new mongoose.Schema({
    username: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    laptops: [{
        name: mongoose.Schema.Types.String,
        id : mongoose.Schema.Types.ObjectId,
        price: mongoose.Schema.Types.Number
    }]
})

const Orders = mongoose.model('Orders', ordersSchema);

module.exports = Orders;
