const mongoose = require('mongoose');

const favouritesSchema = new mongoose.Schema({
    username: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    laptops: [{
        name: mongoose.Schema.Types.String,
        id : mongoose.Schema.Types.ObjectId
    }]
})

const Favourites = mongoose.model('Favourites', favouritesSchema);

module.exports = Favourites;
