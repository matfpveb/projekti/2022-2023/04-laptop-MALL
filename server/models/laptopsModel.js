const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { filterByBrend } = require('../controllers/laptops');


const laptopsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    name: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    brand: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    processor: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    ram: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    ssd: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    price: {
        type: mongoose.Schema.Types.Number,
        required: true  
    },
    imgUrl: {
        type: mongoose.Schema.Types.String,
        required: true
    }
}, {
    versionKey: false
});


laptopsSchema.plugin(mongoosePaginate);

const laptopsModel = mongoose.model('laptops', laptopsSchema);

async function paginateThroughProducts(page = 1, limit = 9) {
    return await laptopsModel.paginate({}, { page, limit });
}

async function paginateThroughProductsName(page = 1, limit = 9, name) {
    return await laptopsModel.paginate({name : {$eq: name}}, { page, limit });
}


async function paginateThroughProductsFILTER(page = 1, limit = 9, brend, ram, processor, price, ssd) {
    return await laptopsModel.paginate({ 
        $and:[
                {brand: {$in: brend}},
                {ram: {$in: ram}},
                {processor: {$in: processor}},
                {price: {$lte: price}},
                {ssd: {$in: ssd}}
        ]},{ page, limit });
}
  
  
module.exports = {
    laptopsModel,
    paginateThroughProducts,
    paginateThroughProductsFILTER, 
    paginateThroughProductsName
};

